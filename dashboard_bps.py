from numpy.core.fromnumeric import size
from numpy.lib.function_base import select
import streamlit as st
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import altair as alt
import re
import matplotlib.pyplot as plt
import base64
import streamlit.components.v1 as stc
import os
import plotly.express as px
from matplotlib.figure import Figure
from plotly import graph_objs as go
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor,ExtraTreesRegressor
import pydea as dea

LOGO_IMAGE = "./bps.png"
# #Baca Dataset awal
data = pd.read_excel('./dataset/raw.xlsx')
data_train = pd.read_excel('./dataset/clean_bps.xlsx')
data_kedua = pd.read_excel('./dataset/clean_bps_cad.xlsx')
#Hitung rata-rata gaji
data['rata_rata_gaji'] = (data['salary_max'] + data['salary_min']) / 2
#Ubah ke Kuartal
data['tanggal'] = pd.to_datetime(data['ads_start'])
data['kuarter'] = data['ads_start'].dt.to_period('Q')
# membuat varible baru bernama cut_points
# merupakan batas dari setiap kategori
cut_points = [0, 2000000, 4000000, 6000000, 8000000, 100000000]
# membuat label atas kategorinya
salary_label = ['0 - 2juta', '2juta - 4 juta', '4juta - 6juta', '6juta - 8 juta', 'diatas 8 juta']
# implementasi label tersebut dengan membuat kolom baru bernama age_label
data['kelompok_gaji'] = pd.cut(data['rata_rata_gaji'], bins=cut_points, labels=salary_label, include_lowest=True)
#Disable Warning
st.set_option('deprecation.showPyplotGlobalUse', False)
#Set Size
sns.set(rc={'figure.figsize':(8,8)})
#Coloring
colors_1 = ['#66b3ff','#99ff99']
colors_2 = ['#66b3ff','#99ff99']
colors_3 = ['#79ff4d','#4d94ff']
colors_4 = ['#ff0000','#ff1aff']
st.markdown(
    f"""
    <div style="text-align: center;">
    <img class="logo-img" src="data:png;base64,{base64.b64encode(open(LOGO_IMAGE, 'rb').read()).decode()}">
    </div>
    """,
    unsafe_allow_html=True
)
st.markdown("<h1 style='text-align: center; color: #243A74; font-family:sans-serif'>Job Market Analysis: Sectoral, Geographical, and Salary Prediction</h1>", unsafe_allow_html=True)

menu_utama = st.sidebar.selectbox('Pilih Menu', ('Dashboard','Prediksi'))
if menu_utama == 'Dashboard':
    radio_filter = st.sidebar.radio('Pilih Filter : ', ('Job Category', 'Company Industry','Lokasi'))
    #Filter Job Category
    if radio_filter == 'Job Category' :
        filter_job = st.selectbox("Pilih Kategori Pekerjaan", data['category'].unique())
        for item in data['category'].unique():
            if item == filter_job:
                st.write('### Data Kategori Pekerjaan ' + filter_job)
        data_plot_category = data[(data.category==filter_job)]
        data_plot_all = data.groupby('kuarter').mean()['rata_rata_gaji']/1000000
        data_plot_per = data[(data.category==filter_job)].groupby('kuarter').mean()['rata_rata_gaji']/1000000
        col1, col2 = st.beta_columns(2)
        with col1:
            st.write("Tren Rata-Rata Gaji Dalam Jutaan " + filter_job)
            data_plot_per.plot(color='blue', marker='o')
            data_plot_all.plot(color='black',marker='o')
            plt.title('Tren Gaji Rata-Rata', fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Rata-Rata Gaji (dalam juta)', fontsize=12)
            plt.grid(True)
            plt.legend()
            st.pyplot()
        with col2 :
            st.write("Tren Jumlah Penawaran Kerja " + filter_job)
            df_kategori = []
            df_all = []
            for i in data['kuarter'].unique() :
                a = data[(data.category == filter_job) & (data.kuarter==i)].count()['kuarter']
                df_kategori.append(a)
            for j in data['kuarter'].unique() :
                b = data[(data.kuarter==j)].count()['kuarter']
                df_all.append(b)
            data_all = pd.DataFrame({'Periode' : data['kuarter'].unique(),'Jumlah Tawaran Kerja Agregat': df_all})
            data_siap = pd.DataFrame({'Periode' : data['kuarter'].unique(),'Jumlah Tawaran Kerja ' + filter_job : df_kategori})
            data_siap.plot(color='red', marker='o')
            # data_all.plot(color='black',marker='o')
            plt.title('Tren Jumlah Penawaran Kerja', fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Jumlah Penawaran', fontsize=12)
            plt.grid(True)
            plt.legend()
            st.pyplot()
        col3, col4 = st.beta_columns(2)
        with col3:
            st.write('Sebaran Gaji Tahun 2019-2021 dari Pekerjaan '+ filter_job)
            labels = data[(data.category==filter_job)]['kelompok_gaji'].value_counts().index
            values = data[(data.category==filter_job)]['kelompok_gaji'].value_counts().values
            fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
            st.plotly_chart(fig)
        with col4:
            sorted_q = sorted (data['kuarter'].unique())
            df_agre = []
            for k in sorted_q :
                d = data[(data.category == filter_job) & (data.kuarter==k)].count()['kuarter']
                df_agre.append(d)
            df_all_agre = []
            for l in sorted_q :
                e = data[(data.kuarter==l)].count()['kuarter']
                df_all_agre.append(e)
            res = [i / j for i, j in zip(df_agre, df_all_agre)]
            persen = []
            for item in res:
                item_i = item*100
                persen.append(item_i)
            data_ok = pd.DataFrame({'Periode' : sorted_q,'Persentase': persen})
            pd.options.display.float_format = "{:,.2f}".format
            data_ok.Periode = data_ok.Periode.astype(str)
            x=data_ok.Periode
            y=data_ok['Persentase']
            sns.set_style("whitegrid")
            # Color palette
            blue, = sns.color_palette("muted", 1)
            # Area plot
            fig, ax = plt.subplots()
            plt.title('Kontribusi Terhadap Pasar TK Nasional dari Jenis Pekerjaan ' + filter_job, fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Rata-Rata Jumlah Tawaran Kerja (dalam persen)', fontsize=12)
            ax.plot(x, y, color=blue, lw=3)
            ax.fill_between(x, 0, y, alpha=.3)
            ax.set(xlim=(0, len(x) - 1), ylim=(0, None), xticks=x)
            st.pyplot(fig)
    #Filter Company Industry
    if radio_filter == 'Company Industry' :
        filter_industry = st.selectbox("Pilih Kategori Industi", data['company_industry'].unique())
        for item in data['company_industry'].unique():
            if item == filter_industry:
                st.write('### Data Kategori Industri ' + filter_industry)
        data_plot_category = data[(data.company_industry==filter_industry)]
        data_plot_all = data.groupby('kuarter').mean()['rata_rata_gaji']/1000000
        data_plot_per = data[(data.company_industry==filter_industry)].groupby('kuarter').mean()['rata_rata_gaji']/1000000
        col1, col2 = st.beta_columns(2)
        with col1:
            st.write("Tren Rata-Rata Gaji Dalam Jutaan")
            data_plot_per.plot(color='blue', marker='o')
            data_plot_all.plot(color='black',marker='o')
            plt.title('Tren Gaji Rata-Rata', fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Rata-Rata Gaji (dalam juta)', fontsize=12)
            plt.grid(True)
            plt.legend()
            st.pyplot()
        with col2 :
            st.write("Tren Jumlah Penawaran Kerja")
            df_kategori = []
            df_all = []
            for i in data['kuarter'].unique() :
                a = data[(data.company_industry == filter_industry) & (data.kuarter==i)].count()['kuarter']
                df_kategori.append(a)
            for j in data['kuarter'].unique() :
                b = data[(data.kuarter==j)].count()['kuarter']
                df_all.append(b)
            data_all = pd.DataFrame({'Periode' : data['kuarter'].unique(),'Jumlah Tawaran Kerja Agregat': df_all})
            data_siap = pd.DataFrame({'Periode' : data['kuarter'].unique(),'Jumlah Tawaran Kerja ' + filter_industry : df_kategori})
            data_siap.plot(color='red', marker='o')
            # data_all.plot(color='black',marker='o')
            plt.title('Tren Jumlah Penawaran Kerja di Industri ' + filter_industry, fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Jumlah Penawaran', fontsize=12)
            plt.grid(True)
            plt.legend()
            st.pyplot()
        col3, col4 = st.beta_columns(2)
        with col3:
            st.write('Sebaran Gaji Tahun 2019-2021 di Industri ' + filter_industry)
            labels = data[(data.company_industry==filter_industry)]['kelompok_gaji'].value_counts().index
            values = data[(data.company_industry==filter_industry)]['kelompok_gaji'].value_counts().values
            fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
            st.plotly_chart(fig)
        with col4:
            sorted_q = sorted (data['kuarter'].unique())
            df_agre = []
            for k in sorted_q :
                d = data[(data.company_industry == filter_industry) & (data.kuarter==k)].count()['kuarter']
                df_agre.append(d)
            df_all_agre = []
            for l in sorted_q :
                e = data[(data.kuarter==l)].count()['kuarter']
                df_all_agre.append(e)
            res = [i / j for i, j in zip(df_agre, df_all_agre)]
            persen = []
            for item in res:
                item_i = item*100
                persen.append(item_i)
            data_ok = pd.DataFrame({'Periode' : sorted_q,'Persentase': persen})
            pd.options.display.float_format = "{:,.2f}".format
            data_ok.Periode = data_ok.Periode.astype(str)
            x=data_ok.Periode
            y=data_ok['Persentase']
            sns.set_style("whitegrid")
            # Color palette
            blue, = sns.color_palette("muted", 1)
            # Area plot
            fig, ax = plt.subplots()
            plt.title('Kontribusi Terhadap Pasar TK Nasional dari Industri ' + filter_industry, fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Rata-Rata Jumlah Tawaran Kerja (dalam persen)', fontsize=12)
            ax.plot(x, y, color=blue, lw=3)
            ax.fill_between(x, 0, y, alpha=.3)
            ax.set(xlim=(0, len(x) - 1), ylim=(0, None), xticks=x)
            st.pyplot(fig)
    #Filter Locations
    if radio_filter == 'Lokasi' :
        filter_lokasi = st.selectbox("Pilih Lokasi", data['location'].unique())
        for item in data['location'].unique():
            if item == filter_lokasi:
                st.write('### Data Kategori Wilayah ' + filter_lokasi)
        data_plot_category = data[(data.location==filter_lokasi)]
        data_plot_all = data.groupby('kuarter').mean()['rata_rata_gaji']/1000000
        data_plot_per = data[(data.location==filter_lokasi)].groupby('kuarter').mean()['rata_rata_gaji']/1000000
        col1, col2 = st.beta_columns(2)
        with col1:
            st.write("Tren Rata-Rata Gaji Dalam Jutaan pada Wilayah " + filter_lokasi)
            data_plot_per.plot(color='blue', marker='o')
            data_plot_all.plot(color='black',marker='o')
            plt.title('Tren Gaji Rata-Rata', fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Rata-Rata Gaji (dalam juta)', fontsize=12)
            plt.grid(True)
            plt.legend()
            st.pyplot()
        with col2 :
            st.write("Tren Jumlah Penawaran Kerja pada Wilayah " + filter_lokasi)
            df_kategori = []
            df_all = []
            for i in data['kuarter'].unique() :
                a = data[(data.location == filter_lokasi) & (data.kuarter==i)].count()['kuarter']
                df_kategori.append(a)
            for j in data['kuarter'].unique() :
                b = data[(data.kuarter==j)].count()['kuarter']
                df_all.append(b)
            data_all = pd.DataFrame({'Periode' : data['kuarter'].unique(),'Jumlah Tawaran Kerja Agregat': df_all})
            data_siap = pd.DataFrame({'Periode' : data['kuarter'].unique(),'Jumlah Tawaran Kerja ' + filter_lokasi : df_kategori})
            data_siap.plot(color='red', marker='o')
            # data_all.plot(color='black',marker='o')
            plt.title('Tren Jumlah Penawaran Kerja di Industri ' + filter_lokasi, fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Jumlah Penawaran', fontsize=12)
            plt.grid(True)
            plt.legend()
            st.pyplot()
        col3, col4 = st.beta_columns(2)
        with col3:
            st.write('Sebaran Gaji Tahun 2019-2021 di Lokasi ' + filter_lokasi)
            labels = data[(data.location==filter_lokasi)]['kelompok_gaji'].value_counts().index
            values = data[(data.location==filter_lokasi)]['kelompok_gaji'].value_counts().values
            fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
            st.plotly_chart(fig)
        with col4:
            sorted_q = sorted (data['kuarter'].unique())
            df_agre = []
            for k in sorted_q :
                d = data[(data.location == filter_lokasi) & (data.kuarter==k)].count()['kuarter']
                df_agre.append(d)
            df_all_agre = []
            for l in sorted_q :
                e = data[(data.kuarter==l)].count()['kuarter']
                df_all_agre.append(e)
            res = [i / j for i, j in zip(df_agre, df_all_agre)]
            persen = []
            for item in res:
                item_i = item*100
                persen.append(item_i)
            data_ok = pd.DataFrame({'Periode' : sorted_q,'Persentase': persen})
            pd.options.display.float_format = "{:,.2f}".format
            data_ok.Periode = data_ok.Periode.astype(str)
            x=data_ok.Periode
            y=data_ok['Persentase']
            sns.set_style("whitegrid")
            # Color palette
            blue, = sns.color_palette("muted", 1)
            # Area plot
            fig, ax = plt.subplots()
            plt.title('Kontribusi Terhadap Pasar TK Nasional dari Wilayah ' + filter_lokasi, fontsize=12)
            plt.xlabel('Kuartal', fontsize=12)
            plt.ylabel('Rata-Rata Jumlah Tawaran Kerja (dalam persen)', fontsize=12)
            ax.plot(x, y, color=blue, lw=3)
            ax.fill_between(x, 0, y, alpha=.3)
            ax.set(xlim=(0, len(x) - 1), ylim=(0, None), xticks=x)
            st.pyplot(fig)
if menu_utama == 'Prediksi':
    radio_filter = st.sidebar.radio('Pilih Menu : ', ('Fitting Model', 'Prediksi'))
    if radio_filter == 'Fitting Model':
        st.write('## Train Model')
        radio_model = st.radio('Fitting Model',('Decision Tree','Linear Regression', 'Random Forest'))
        if radio_model == 'Decision Tree':
            st.write('## Decision Tree')
        if radio_model == 'Linear Regression':
            st.write('## Linear Regression')
        if radio_model == 'Random Forest':
            st.write('## Random Forest')
            #Split Kolom location
            # inisiasi encoder 
            ohc = OneHotEncoder(handle_unknown='ignore')
            # fit dan transform
            new_features = ohc.fit_transform(data_train[['location']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in data_train['location'].unique()]
            loc = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([data_train,loc], axis=1)
            #Split Kolom experience
            # fit dan transform
            new_features = ohc.fit_transform(job_clean[['experience']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in job_clean['experience'].unique()]
            exp = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([job_clean,exp], axis=1)
            #Split Kolom category
            # fit dan transform
            new_features = ohc.fit_transform(job_clean[['category']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in job_clean['category'].unique()]
            cat = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([job_clean,cat], axis=1)
            #Split Kolom company_industry
            # fit dan transform
            new_features = ohc.fit_transform(job_clean[['company_industry']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in job_clean['company_industry'].unique()]
            company_industry = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([job_clean,company_industry], axis=1)
            #Split Kolom company_size
            # fit dan transform
            new_features = ohc.fit_transform(job_clean[['company_size']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in job_clean['company_size'].unique()]
            company_size = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([job_clean,company_size], axis=1)
            #Split Kolom pend_min
            # fit dan transform
            new_features = ohc.fit_transform(job_clean[['pend_min']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in job_clean['pend_min'].unique()]
            pend_min = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([job_clean,pend_min], axis=1)
            #Split Kolom Year
            # fit dan transform
            new_features = ohc.fit_transform(job_clean[['Year']])
            # masukan ke dataframe hasilnya
            cols_name= [x for x in job_clean['Year'].unique()]
            Year = pd.DataFrame(new_features.toarray(), columns=cols_name)
            # gabung hasilnya ke job_clean
            job_clean = pd.concat([job_clean,Year], axis=1)
            job_clean_ok = job_clean.drop(['location','experience','category','company_industry','company_size','pend_min','Year'],axis=1)
            #define X & y
            X = job_clean_ok.drop(['salary_ave'], axis=1)
            y = job_clean_ok['salary_ave']
            #Define Model
            model_rf = RandomForestRegressor(bootstrap=True, ccp_alpha=0.0, criterion='mse',
                      max_depth=None, max_features='auto', max_leaf_nodes=None,
                      max_samples=None, min_impurity_decrease=0.0,
                      min_impurity_split=None, min_samples_leaf=1,
                      min_samples_split=2, min_weight_fraction_leaf=0.0,
                      n_estimators=100, n_jobs=-1, oob_score=False,
                      random_state=4404, verbose=0, warm_start=False)
            if st.button("Fit Model"):
                model_rf.fit(X,y)
    if radio_filter == 'Prediksi':
        cola, colb = st.beta_columns(2)
        with cola:
            st.write('## Pekerjaan 1')
            pilih_lokasi_1 = st.selectbox('Pilih Lokasi',data_train['location'].unique())
            for item1 in data_train['location'].unique():
                if item1 == pilih_lokasi_1:
                    st.write(pilih_lokasi_1)
            pilih_pengalaman_1 = st.selectbox('Pilih Pengalaman',data_train['experience'].unique())
            for item2 in data_train['experience'].unique():
                if item2 == pilih_pengalaman_1:
                    st.write(pilih_pengalaman_1)
            pilih_kategori_1 = st.selectbox('Pilih Kategori Pekerjaan',data_train['category'].unique())
            for item3 in data_train['category'].unique():
                if item3 == pilih_kategori_1:
                    st.write(pilih_kategori_1)
            pilih_industri_1 = st.selectbox('Pilih Industri Perusahaan',data_train['company_industry'].unique())
            for item4 in data_train['company_industry'].unique():
                if item4 == pilih_industri_1:
                    st.write(pilih_industri_1)
            pilih_ukuran_1 = st.selectbox('Pilih Ukuran Perusahaan',data_train['company_size'].unique())
            for item5 in data_train['company_size'].unique():
                if item5 == pilih_ukuran_1:
                    st.write(pilih_ukuran_1)
            pilih_pendidikan_1 = st.selectbox('Pilih Pendidikan',data_train['pend_min'].unique())
            for item6 in data_train['pend_min'].unique():
                if item6 == pilih_pendidikan_1:
                    st.write(pilih_pendidikan_1)
            pilih_tahun_1 = st.selectbox('Pilih Tahun',data_train['Year'].unique())
            for item7 in data_train['Year'].unique():
                if item7 == pilih_tahun_1:
                    st.write(pilih_tahun_1)
            if st.button('Prediksi Gaji Pekerjaan 1'):
                #Split Kolom location
                # inisiasi encoder 
                ohc = OneHotEncoder(handle_unknown='ignore')
                # fit dan transform
                new_features = ohc.fit_transform(data_train[['location']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in data_train['location'].unique()]
                loc = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([data_train,loc], axis=1)
                #Split Kolom experience
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['experience']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['experience'].unique()]
                exp = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,exp], axis=1)
                #Split Kolom category
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['category']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['category'].unique()]
                cat = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,cat], axis=1)
                #Split Kolom company_industry
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['company_industry']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['company_industry'].unique()]
                company_industry = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,company_industry], axis=1)
                #Split Kolom company_size
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['company_size']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['company_size'].unique()]
                company_size = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,company_size], axis=1)
                #Split Kolom pend_min
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['pend_min']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['pend_min'].unique()]
                pend_min = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,pend_min], axis=1)
                #Split Kolom Year
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['Year']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['Year'].unique()]
                Year = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,Year], axis=1)
                job_clean_ok = job_clean.drop(['location','experience','category','company_industry','company_size','pend_min','Year'],axis=1)
                #define X & y
                X = job_clean_ok.drop(['salary_ave'], axis=1)
                y = job_clean_ok['salary_ave']
                index=[0]
                df_1_pred = pd.DataFrame({
                    'location' : pilih_lokasi_1,
                    'experience' : pilih_pengalaman_1,
                    'category' : pilih_kategori_1,
                    'company_industry':pilih_industri_1,
                    'company_size': pilih_ukuran_1,
                    'pend_min' : pilih_pendidikan_1,
                    'Year' : pilih_tahun_1
                },index=index)
                #Set semua nilai jadi 0
                df_kosong_1 = X[:1]
                for col in df_kosong_1.columns:
                    df_kosong_1[col].values[:] = 0
                list_1 = []
                for i in df_1_pred.columns:
                    x = df_1_pred[i][0]
                    list_1.append(x)
                #buat dataset baru
                for i in df_kosong_1.columns:
                    for j in list_1:
                        if i == j:
                            df_kosong_1[i] = df_kosong_1[i].replace(df_kosong_1[i].values,1)
                model_rf = RandomForestRegressor(bootstrap=True, ccp_alpha=0.0, criterion='mse',
                      max_depth=None, max_features='auto', max_leaf_nodes=None,
                      max_samples=None, min_impurity_decrease=0.0,
                      min_impurity_split=None, min_samples_leaf=1,
                      min_samples_split=2, min_weight_fraction_leaf=0.0,
                      n_estimators=100, n_jobs=-1, oob_score=False,
                      random_state=4404, verbose=0, warm_start=False)
                model_rf.fit(X,y)
                pred_1 = model_rf.predict(df_kosong_1)
                st.write('Prediksi Gaji Pekerjaan 1 adalah : ', pred_1)
        with colb:
            st.write('## Pekerjaan 2')
            data_baru_1 = data_train[data_train.location != item1]
            data_baru_2 = data_train[data_train.experience != item2]
            data_baru_3 = data_train[data_train.category != item3]
            data_baru_4 = data_train[data_train.company_industry != item4]
            data_baru_5 = data_train[data_train.company_size != item5]
            data_baru_6 = data_train[data_train.pend_min != item6]
            data_baru_7 = data_train[data_train.Year != item7]
            pilih_lokasi_2 = st.selectbox('Pilih Lokasi',data_baru_1['location'].unique())
            st.write(pilih_lokasi_2)
            pilih_pengalaman_2 = st.selectbox('Pilih Pengalaman',data_baru_2['experience'].unique())
            st.write(pilih_pengalaman_2)
            pilih_kategori_2 = st.selectbox('Pilih Kategori Pekerjaan',data_baru_3['category'].unique())
            st.write(pilih_kategori_2)
            pilih_industri_2 = st.selectbox('Pilih Industri Perusahaan',data_baru_4['company_industry'].unique())
            st.write(pilih_industri_2)
            pilih_ukuran_2 = st.selectbox('Pilih Ukuran Perusahaan',data_baru_5['company_size'].unique())
            st.write(pilih_ukuran_2)
            pilih_pendidikan_2 = st.selectbox('Pilih Pendidikan',data_baru_6['pend_min'].unique())
            st.write(pilih_pendidikan_2)
            pilih_tahun_2 = st.selectbox('Pilih Tahun',data_baru_7['Year'].unique())
            st.write(pilih_tahun_2)
            if st.button('Prediksi Gaji Pekerjaan 2'):
                #Split Kolom location
                # inisiasi encoder 
                ohc = OneHotEncoder(handle_unknown='ignore')
                # fit dan transform
                new_features = ohc.fit_transform(data_train[['location']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in data_train['location'].unique()]
                loc = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([data_train,loc], axis=1)
                #Split Kolom experience
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['experience']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['experience'].unique()]
                exp = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,exp], axis=1)
                #Split Kolom category
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['category']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['category'].unique()]
                cat = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,cat], axis=1)
                #Split Kolom company_industry
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['company_industry']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['company_industry'].unique()]
                company_industry = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,company_industry], axis=1)
                #Split Kolom company_size
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['company_size']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['company_size'].unique()]
                company_size = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,company_size], axis=1)
                #Split Kolom pend_min
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['pend_min']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['pend_min'].unique()]
                pend_min = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,pend_min], axis=1)
                #Split Kolom Year
                # fit dan transform
                new_features = ohc.fit_transform(job_clean[['Year']])
                # masukan ke dataframe hasilnya
                cols_name= [x for x in job_clean['Year'].unique()]
                Year = pd.DataFrame(new_features.toarray(), columns=cols_name)
                # gabung hasilnya ke job_clean
                job_clean = pd.concat([job_clean,Year], axis=1)
                job_clean_ok = job_clean.drop(['location','experience','category','company_industry','company_size','pend_min','Year'],axis=1)
                #define X & y
                X = job_clean_ok.drop(['salary_ave'], axis=1)
                y = job_clean_ok['salary_ave']
                index=[0, 1, 2, 3, 4, 5, 6]
                df_2_pred = pd.DataFrame({
                    'location' : pilih_lokasi_2,
                    'experience' : pilih_pengalaman_2,
                    'category' : pilih_kategori_2,
                    'company_industry':pilih_industri_2,
                    'company_size': pilih_ukuran_2,
                    'pend_min' : pilih_pendidikan_2,
                    'Year' : pilih_tahun_2
                },index=index)
                #Set semua nilai jadi 0
                df_kosong_2 = X[:1]
                for col in df_kosong_2.columns:
                    df_kosong_2[col].values[:] = 0
                #buat dataset baru
                list_2 = []
                for i in df_2_pred.columns:
                    x = df_2_pred[i][0]
                    list_2.append(x)
                #buat dataset baru
                for i in df_kosong_2.columns:
                    for j in list_2:
                        if i == j:
                            df_kosong_2[i] = df_kosong_2[i].replace(df_kosong_2[i].values,1)
                model_rf = RandomForestRegressor(bootstrap=True, ccp_alpha=0.0, criterion='mse',
                      max_depth=None, max_features='auto', max_leaf_nodes=None,
                      max_samples=None, min_impurity_decrease=0.0,
                      min_impurity_split=None, min_samples_leaf=1,
                      min_samples_split=2, min_weight_fraction_leaf=0.0,
                      n_estimators=100, n_jobs=-1, oob_score=False,
                      random_state=4404, verbose=0, warm_start=False)
                model_rf.fit(X,y)
                pred_2 = model_rf.predict(df_kosong_2)
                st.write('Prediksi Gaji Pekerjaan 2 adalah : ', pred_2)
